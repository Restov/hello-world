package com.vfadin.pmu1

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun maxTest() {
        assertEquals(4, MinMax.max(4,2))
        assertEquals(-1, MinMax.max(-1,-2))
        assertEquals(2, MinMax.max(2,2))
        assertEquals(1, MinMax.max(1,-1))
    }

    @Test
    fun minTest() {
        assertEquals(2, MinMax.min(2,4))
        assertEquals(-2, MinMax.min(-2,-1))
        assertEquals(2, MinMax.min(2,2))
        assertEquals(-1, MinMax.min(1, -1))
    }
}