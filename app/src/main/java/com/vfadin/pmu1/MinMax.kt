package com.vfadin.pmu1

class MinMax<T> {
    companion object {
        /**Fun to compare two Int's
         * @param obj1 Int to be compared with obj2
         *
         * @return greater of two integers
         */
        fun max(obj1: Int, obj2: Int) =
            if (obj1 > obj2) obj1
            else obj2

        /**Fun to compare two Int's
         * @param obj1 Int to be compared with obj2
         *
         * @return least of two integers
         */
        fun min(obj1: Int, obj2: Int) =
            if (obj1 > obj2) obj2
            else obj1
    }
}